#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals  # إضافة هذا السطر
import os
import re
import argparse
import logging
from yt_dlp import YoutubeDL
from pydub import AudioSegment  # استخدام pydub بدلاً من moviepy

# قاموس الحروف العربية والحروف المقابلة لها بالأبجدية الإنجليزية
ARABIC_TO_ENGLISH = {
    'ا': 'a', 'أ': 'a', 'إ': 'a', 'آ': 'a',
    'ب': 'b', 'ت': 't', 'ث': 'th', 'ج': 'j',
    'ح': 'h', 'خ': 'kh', 'د': 'd', 'ذ': 'th',
    'ر': 'r', 'ز': 'z', 'س': 's', 'ش': 'sh',
    'ص': 's', 'ض': 'd', 'ط': 't', 'ظ': 'z',
    'ع': 'aa', 'غ': 'gh', 'ف': 'f', 'ق': 'q',
    'ك': 'k', 'ل': 'l', 'م': 'm', 'ن': 'n',
    'ه': 'h', 'ة': 'h', 'و': 'w', 'ي': 'y',
    'ى': 'a',  # الألف المقصورة
    'ئ': 'e',  # الهمزة على الياء
    'ؤ': 'o',  # الهمزة على الواو
    'ء': 'a',  # الهمزة
    'ّ': '',   # الشدة (إزالتها)
    'َ': '',   # الفتحة (إزالتها)
    'ُ': '',   # الضمة (إزالتها)
    'ِ': '',   # الكسرة (إزالتها)
    'ْ': '',   # السكون (إزالتها)
}

# إعداد نظام التسجيل (logging)
logging.basicConfig(filename='rename_errors.log', level=logging.ERROR,
                    format='%(asctime)s - %(levelname)s - %(message)s')

# استبدال الحروف العربية بالحروف الإنجليزية المقابلة
def replace_arabic_with_english(text):
    translation_table = str.maketrans(ARABIC_TO_ENGLISH)
    return text.translate(translation_table)

# تنظيف اسم الملف من الأرقام والحروف الخاصة والمسافات الزائدة
def clean_filename(filename):
    # حذف الأرقام والحروف اللاتينية
    filename = re.sub(r'\d+|[a-zA-Z]+', '', filename)
    # حذف علامات التشكيل والحروف الخاصة
    filename = re.sub(r'[^\w\s]', '', filename)
    # استبدال المسافات المتعددة بمسافة واحدة
    filename = re.sub(r'\s+', ' ', filename).strip()
    # استبدال المسافات بشرطة سفلية
    filename = filename.replace(' ', '_')
    return filename

# إعادة تسمية الملف مع التعامل مع الملفات المكررة
def rename_file(file_path, new_file_path):
    try:
        if os.path.exists(new_file_path):
            base, extension = os.path.splitext(new_file_path)
            counter = 1
            while os.path.exists("{}_{}{}".format(base, counter, extension)):
                counter += 1
            new_file_path = "{}_{}{}".format(base, counter, extension)
        os.rename(file_path, new_file_path)
        print("Renamed: {} -> {}".format(os.path.basename(file_path), os.path.basename(new_file_path)))
    except Exception as e:
        logging.error("Error renaming file {}: {}".format(os.path.basename(file_path), e))

# تحميل الفيديو وتحويله إلى MP3 باستخدام pydub
def download_and_convert_to_mp3(url, output_dir):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': os.path.join(output_dir, '%(title)s.%(ext)s'),
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }
    with YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(url, download=True)
        original_file = ydl.prepare_filename(info_dict)
        mp3_file = os.path.splitext(original_file)[0] + '.mp3'
        return mp3_file

# تحديث أسماء جميع الملفات في المجلد المحدد
def rename_files_in_directory(directory):
    print("Scanning directory: {}".format(directory))
    for file_name in os.listdir(directory):
        file_path = os.path.join(directory, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.mp3'):
            print("Processing file: {}".format(file_name))
            new_file_name = clean_filename(file_name)
            new_file_name = replace_arabic_with_english(new_file_name.lower()) + '.mp3'
            new_file_path = os.path.join(directory, new_file_name)
            if file_path != new_file_path:
                print("Renaming to: {}".format(new_file_name))
                rename_file(file_path, new_file_path)
            else:
                print("No need to rename (already in correct format).")
        else:
            print("Skipping non-MP3 file: {}".format(file_name))

# بدء التنفيذ
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Download YouTube videos, convert to MP3, and rename files.")
    parser.add_argument('--url', type=str, help="The YouTube video URL to download.")
    parser.add_argument('--directory', type=str, default=os.path.dirname(os.path.abspath(__file__)),
                        help="The directory to save the downloaded files. Default is the current directory.")
    args = parser.parse_args()

    if args.url:
        print("Downloading and converting: {}".format(args.url))
        mp3_file = download_and_convert_to_mp3(args.url, args.directory)
        print("Downloaded and converted to MP3: {}".format(mp3_file))
        rename_files_in_directory(args.directory)
        print("Renaming completed successfully!")
    else:
        print("Please provide a YouTube URL using --url.")
    print("Script by Fahad ALGhathbar")