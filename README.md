# Arabic-to-English-Letters

Convert Arabic file names with Arabic characters to Arabic names with English letters.

تحويل أسماء الملفات العربية ذات الأحرف العربية إلى أسماء عربية ذات أحرف إنجليزية.

---

## Description | الوصف

This repository contains two Python scripts:

1. **Simple Script**: Renames Arabic file names by converting Arabic characters to English letters.
2. **Advanced Script**: Downloads YouTube videos or playlists, converts them to MP3, and renames the files by converting Arabic characters to English letters.

يحتوي هذا المستودع على سكربتين بايثون:

1. **سكربت بسيط**: يعيد تسمية الملفات العربية عن طريق تحويل الأحرف العربية إلى أحرف إنجليزية.
2. **سكربت متطور**: يقوم بتحميل مقاطع الفيديو أو قوائم التشغيل من YouTube، وتحويلها إلى MP3، ثم إعادة تسمية الملفات عن طريق تحويل الأحرف العربية إلى أحرف إنجليزية.

---

## Features | المميزات

### Simple Script | السكربت البسيط

- Convert Arabic file names to English letters.
  - تحويل أسماء الملفات العربية إلى أحرف إنجليزية.
- Works with `.mp3` files in the current directory.
  - يعمل مع ملفات `.mp3` في المجلد الحالي.

### Advanced Script | السكربت المتطور

- Download YouTube videos or playlists.
  - تحميل مقاطع الفيديو أو قوائم التشغيل من YouTube.
- Convert downloaded videos to MP3.
  - تحويل مقاطع الفيديو المحملة إلى MP3.
- Automatically rename files after conversion.
  - إعادة تسمية الملفات تلقائيًا بعد التحويل.

---

## Requirements | المتطلبات

### Simple Script | السكربت البسيط

- Python 3.x

### Advanced Script | السكربت المتطور

- Python 3.x
- `yt-dlp`
- `pydub`
- FFmpeg

---

## Installation | التثبيت

### Simple Script | السكربت البسيط

1. Install Python 3.x from [python.org](https://www.python.org/downloads/).

### Advanced Script | السكربت المتطور

1. Install Python 3.x from [python.org](https://www.python.org/downloads/).
2. Install the required libraries using pip:

   ```bash
   pip install yt-dlp pydub
   ```
3. Install FFmpeg from [ffmpeg.org](https://ffmpeg.org/).

---

## Usage | الاستخدام

### Simple Script | السكربت البسيط

1. Place the script (`arabic.py`) in the directory containing the `.mp3` files.
2. Run the script:

   ```bash
   python arabic.py
   ```

### Advanced Script | السكربت المتطور

1. Clone the repository or download the script.
2. Run the script with the following command:

   ```bash
   python advanced_arabic.py --url "YOUTUBE_URL" --directory "OUTPUT_DIRECTORY"
   ```

   - Replace `YOUTUBE_URL` with the URL of the YouTube video or playlist.
   - Replace `OUTPUT_DIRECTORY` with the directory where you want to save the files.

#### Example | مثال

```bash
python advanced_arabic.py --url "https://youtube.com/playlist?list=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" --directory audio/
```

---

## How It Works | كيف يعمل

### Simple Script | السكربت البسيط

#### 1. Scan Directory

The script scans the current directory for `.mp3` files.

يقوم السكربت بفحص المجلد الحالي للبحث عن ملفات `.mp3`.

#### 2. Rename Files

The script renames the files by converting Arabic characters to English letters.

يقوم السكربت بإعادة تسمية الملفات عن طريق تحويل الأحرف العربية إلى أحرف إنجليزية.

### Advanced Script | السكربت المتطور

#### 1. Download YouTube Videos

The script uses `yt-dlp` to download videos or playlists from YouTube.

يتم استخدام `yt-dlp` لتحميل مقاطع الفيديو أو قوائم التشغيل من YouTube.

#### 2. Convert Videos to MP3

The script converts the downloaded videos to MP3 using FFmpeg.

يتم تحويل مقاطع الفيديو المحملة إلى MP3 باستخدام FFmpeg.

#### 3. Rename Files

The script renames the MP3 files by converting Arabic characters to English letters.

يتم إعادة تسمية ملفات MP3 عن طريق تحويل الأحرف العربية إلى أحرف إنجليزية.

---

## Special Thanks | شكر خاص

Special thanks to Engineer Fahad Al-Duraibi for his contributions.

شكر خاص للمهندس فهد الدريبي على مساهماته.

- GitHub: [fduraibi](https://github.com/fduraibi)
- WEBSITE: [fadvisor](https://www.fadvisor.net/blog)

---

## License | الترخيص

This project is licensed under the MIT License. See the `LICENSE` file for details.
