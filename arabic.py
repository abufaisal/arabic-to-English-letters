#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re

# قائمة الحروف العربية والحروف المقابلة لها بالأبجدية الإنجليزية
ARABIC_TO_ENGLISH = {
    'ا': 'a', 'أ': 'a', 'إ': 'a', 'آ': 'a',
    'ب': 'b', 'ت': 't', 'ث': 'th', 'ج': 'j',
    'ح': 'h', 'خ': 'kh', 'د': 'd', 'ذ': 'th',
    'ر': 'r', 'ز': 'z', 'س': 's', 'ش': 'sh',
    'ص': 's', 'ض': 'd', 'ط': 't', 'ظ': 'z',
    'ع': 'aa', 'غ': 'gh', 'ف': 'f', 'ق': 'q',
    'ك': 'k', 'ل': 'l', 'م': 'm', 'ن': 'n',
    'ه': 'h', 'ة': 'h', 'و': 'w', 'ي': 'y',
    'ى': 'a',  # الألف المقصورة
    'ئ': 'e',  # الهمزة على الياء
    'ؤ': 'o',  # الهمزة على الواو
    'ء': 'a',  # الهمزة
    'ّ': '',   # الشدة (إزالتها)
    'َ': '',   # الفتحة (إزالتها)
    'ُ': '',   # الضمة (إزالتها)
    'ِ': '',   # الكسرة (إزالتها)
    'ْ': '',   # السكون (إزالتها)
}

# استبدال الحروف العربية بالحروف الإنجليزية المقابلة
def replace_arabic_with_english(text):
    translation_table = str.maketrans(ARABIC_TO_ENGLISH)
    return text.translate(translation_table)

# تنظيف اسم الملف من الأرقام والحروف الخاصة والمسافات الزائدة
def clean_filename(filename):
    # حذف الأرقام والحروف اللاتينية
    filename = re.sub(r'\d+|[a-zA-Z]+', '', filename)
    # حذف علامات التشكيل والحروف الخاصة
    filename = re.sub(r'[^\w\s]', '', filename)
    # استبدال المسافات المتعددة بمسافة واحدة
    filename = re.sub(r'\s+', ' ', filename).strip()
    # استبدال المسافات بشرطة سفلية
    filename = filename.replace(' ', '_')
    return filename

# إعادة تسمية الملف مع التعامل مع الملفات المكررة
def rename_file(file_path, new_file_path):
    try:
        if os.path.exists(new_file_path):
            base, extension = os.path.splitext(new_file_path)
            counter = 1
            while os.path.exists("{}_{}{}".format(base, counter, extension)):
                counter += 1
            new_file_path = "{}_{}{}".format(base, counter, extension)
        os.rename(file_path, new_file_path)
        print("Renamed: {} -> {}".format(os.path.basename(file_path), os.path.basename(new_file_path)))
    except Exception as e:
        print("Error renaming file {}: {}".format(os.path.basename(file_path), e))

# تحديث أسماء جميع الملفات في المجلد المحدد
def rename_files_in_directory(directory):
    print("Scanning directory: {}".format(directory))
    for file_name in os.listdir(directory):
        file_path = os.path.join(directory, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.mp3'):
            print("Processing file: {}".format(file_name))
            new_file_name = clean_filename(file_name)
            new_file_name = replace_arabic_with_english(new_file_name.lower()) + '.mp3'
            new_file_path = os.path.join(directory, new_file_name)
            if file_path != new_file_path:
                print("Renaming to: {}".format(new_file_name))
                rename_file(file_path, new_file_path)
            else:
                print("No need to rename (already in correct format).")
        else:
            print("Skipping non-MP3 file: {}".format(file_name))

# بدء التنفيذ
if __name__ == '__main__':
    current_directory = os.path.dirname(os.path.abspath(__file__))
    rename_files_in_directory(current_directory)
    print("Conversion completed successfully!")
    print("Script by Fahad ALGhathbar")